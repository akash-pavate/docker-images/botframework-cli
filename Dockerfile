FROM node:14.17.1-slim

WORKDIR /usr/app

RUN npm install -g npm

RUN npm install -g @microsoft/botframework-cli
